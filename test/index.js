const assert = require("chai").assert;
const expect = require("chai").expect;
const chai = require("chai");
import { RRule, RRuleSet } from "rrule";
import later from "later";
import CustodyPlan, { CustodyBlock } from "../src/custodyPlan";
import Calendar from "../src/calendar";
import moment from "moment";

chai.should();
chai.use(require("chai-things"));

describe("Custody Plan", () => {
  xit("should add 5 days to an empty custody plan", () => {
    let cp = new CustodyPlan();
    let rb = cp.createRangeBlock();
    const now = new Date();
    const uid = 1;
    rb.setCustodyForDate(uid, now);

    rb.setCustodyForDate(uid, moment("20170304"));
    rb.setCustodyForDate(uid, moment("20170307"));
    rb.setCustodyForDate(uid, moment("20171104"));
    rb.setCustodyForDate(uid, moment("20170309"));
    rb.setCustodyForDate(uid, moment("20171311"));

    cp.addBlock(rb);

    const startOfYear = moment("20170101").startOf("year");
    const endOfYear = moment("20170101").endOf("year");
    const dates = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    dates.should.have.length(5);
    dates.should.include.something.that.deep.equals(
      moment("20170304").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170309").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170307").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171104").toDate()
    );
  });

  xit("should add 5 days via an array to an empty custody plan", () => {
    let cp = new CustodyPlan();
    let rb = cp.createRangeBlock();
    const now = new Date();
    const uid = 1;
    rb.setCustodyForDate(uid, now);

    rb.setCustodyForDates(uid, [
      moment("20170304"),
      moment("20170307"),
      moment("20171104"),
      moment("20170309"),
      moment("20171311")
    ]);

    cp.addBlock(rb);

    const startOfYear = moment("20170101").startOf("year");
    const endOfYear = moment("20170101").endOf("year");
    const dates = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    assert(dates.length === 5, "There should be 5 days");
    assert(dates, moment("20170304"));
    assert(dates, moment("20170309"));
    assert(dates, moment("20170307"));
    assert(dates, moment("20171104"));
    assert(dates, moment("20171311"));
  });

  it("should add adjacent days to an empty custody plan and turn them into 3 ranges instead of 8", () => {
    let cp = new CustodyPlan();
    let rb = cp.createRangeBlock();
    const uid = 1;
    rb.setCustodyForDate(uid, moment("20170301"));
    rb.setCustodyForDate(uid, moment("20170302"));
    rb.setCustodyForDate(uid, moment("20170303"));
    rb.setCustodyForDate(uid, moment("20170304"));

    rb.setCustodyForDate(uid, moment("20170801"));
    rb.setCustodyForDate(uid, moment("20170802"));

    rb.setCustodyForDate(uid, moment("20171203"));
    rb.setCustodyForDate(uid, moment("20171204"));

    assert(rb.data.ranges.length === 3, "There should be 3 ranges");
  });

  it("should add adjacent 5 days to empty custody plan and remove the one in the middle", () => {
    let cp = new CustodyPlan();
    let rb = cp.createRangeBlock();
    cp.addBlock(rb);

    const uid = 1;
    const ouid = 2;
    rb.setCustodyForDate(uid, moment("20170301"));
    rb.setCustodyForDate(uid, moment("20170302"));
    rb.setCustodyForDate(uid, moment("20170303"));
    rb.setCustodyForDate(uid, moment("20170304"));
    rb.setCustodyForDate(uid, moment("20170305"));

    assert(rb.data.ranges.length === 1, "There should be 1 block");

    rb.setCustodyForDate(ouid, moment("20170303"));

    assert(rb.data.ranges.length === 3, "There should be 3 blocks");
    const userRange = rb.data.ranges.filter(r => r.userId === uid);
    assert(userRange.length === 2, "There should be 2 block for this user");
    const dates = cp.getCustodyDatesForUser(
      uid,
      moment("20170301"),
      moment("20170305")
    );
    assert(dates.length === 4, "There should be 4");
  });

  it("should merge one added on top of existing custody", () => {
    let cp = new CustodyPlan();
    let rb = cp.createRangeBlock();
    cp.addBlock(rb);
    const uid = 1;
    rb.setCustodyForDate(uid, moment("20170301"));
    rb.setCustodyForDate(uid, moment("20170302"));
    rb.setCustodyForDate(uid, moment("20170303"));
    rb.setCustodyForDate(uid, moment("20170304"));

    assert(rb.data.ranges.length === 1, "There should be 1 range");

    rb.setCustodyForDate(uid, moment("20170302"));

    assert(rb.data.ranges.length === 1, "There should (still) be 1 range");
  });

  it("should create a recurring plan to an empty custody plan", () => {
    let cp = new CustodyPlan();
    const uid = 1;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur.setDate(uid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.setDate(uid, moment("20170329"));
    recur.setDate(uid, moment("20170402"));

    cp.addBlock(recur);

    const startOfYear = moment("20170101").startOf("year");
    const endOfYear = moment("20170101").endOf("year");
    const dates = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170403").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170430").toDate()
    );
  });

  xit("should add day to an empty custody plan and return JSON", () => {
    let cp = new CustodyPlan();
    const year98 = new Date(98, 1);
    cp.setCustodyForDate(year98);
    const json = cp.JSON;
    assert(json.dates === '["RDATE:19980131T230000Z"]');
  });

  xit("should create a custody plan, export to JSON and create the same custody plan from the JSON", () => {
    let cp1 = new CustodyPlan(1); // user #1
    cp1.setCustodyForDate(moment("20170301"));
    cp1.setCustodyForDate(moment("20170302"));
    cp1.setCustodyForDate(moment("20170303"));
    cp1.setCustodyForDate(moment("20170304"));

    const str = cp1.toString();
    let cp2 = CustodyPlan.fromString(str);

    assert(cp1.childId, 1);
    assert(cp2.childId, 1);

    const startOfYear = moment("20170101")
      .startOf("year")
      .toDate();
    const endOfYear = moment("20170101")
      .endOf("year")
      .toDate();

    assert.deepEqual(
      cp1.getCustodyDatesForUser(startOfYear, endOfYear),
      cp2.getCustodyDatesForUser(startOfYear, endOfYear),
      "the 2 CPs plan must be same"
    );
  });

  xit("should toggle a day in an recurring block", () => {
    let cp = new CustodyPlan();
    const uid = 1,
      ouid = 2;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170801"));
    recur.setDate(uid, moment("20170320"));
    recur.toggleDate(uid, ouid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.toggleDate(uid, ouid, moment("20170324"));
    recur.toggleDate(uid, ouid, moment("20170324"));

    cp.addBlock(recur);

    const startOfYear = moment().startOf("year");
    const endOfYear = moment().endOf("year");
    const dates = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
    const odates = cp.getCustodyDatesForUser(
      ouid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
  });

  it("should serialize custody plan with recurring plan", () => {
    let cp = new CustodyPlan();
    const uid = 1;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur.setDate(uid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.setDate(uid, moment("20170329"));
    recur.setDate(uid, moment("20170402"));

    cp.addBlock(recur);

    const startOfYear = moment("20170101").startOf("year");
    const endOfYear = moment("20170101").endOf("year");
    const dates = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170403").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170430").toDate()
    );

    const obj = cp.toObject();
    cp = CustodyPlan.fromObject(obj);
    const datesSerial = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170403").toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170430").toDate()
    );
  });

  it("should create a calendar with custody plans, serialize it and load it again", () => {
    let cal = new Calendar();
    let cp = new CustodyPlan();
    const uid = 1;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur.setDate(uid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.setDate(uid, moment("20170329"));
    recur.setDate(uid, moment("20170402"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);

    const startOfYear = moment("20170101").startOf("year");
    const endOfYear = moment("20170101").endOf("year");
    const dates = cal.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170403").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20170430").toDate()
    );

    const obj = cal.toObject();
    cal = Calendar.fromObject(obj);
    const datesSerial = cal.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170320").toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170403").toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170407").toDate()
    );
    datesSerial.should.include.something.that.deep.equals(
      moment("20170430").toDate()
    );
  });

  xit("should create custody for one user", () => {
    let cp = new CustodyPlan();
    const uid = 1,
      ouid = 2;
    cp.setCustodyForDate(uid, moment("20170301"));
    cp.setCustodyForDate(uid, moment("20170302"));
    cp.setCustodyForDate(uid, moment("20170303"));
    cp.setCustodyForDate(uid, moment("20170304"));

    const startOfYear = moment("20170101").startOf("year");
    const endOfYear = moment("20170101").endOf("year");
    let dates = cp.getCustodyDatesForUser(
      uid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    assert(dates.length === 4, "There should be 5 days");
    dates = cp.getCustodyDatesForUser(
      ouid,
      startOfYear.toDate(),
      endOfYear.toDate()
    );
    assert(dates.length === 0, "There should be 5 days");
  });

  xit("should create 2 custody plans and get dates from both", () => {
    let cp = new CustodyPlan(1);
    let cp2 = new CustodyPlan(2);
    let rb = cp.createRangeBlock();
    let rb2 = cp2.createRangeBlock();
    cp.addBlock(rb);
    cp2.addBlock(rb2);
    const uid = 1;
    rb.setCustodyForDate(uid, moment("20170301"));
    rb.setCustodyForDate(uid, moment("20170302"));
    rb.setCustodyForDate(uid, moment("20170303"));
    rb2.setCustodyForDate(uid, moment("20170303"));
    rb2.setCustodyForDate(uid, moment("20170304"));

    let calendar = new Calendar();
    calendar.addCustodyPlan(cp);
    calendar.addCustodyPlan(cp2);

    const dates = calendar.getCustodyDatesForUser(
      uid,
      moment("20170301"),
      moment("20170304")
    );
    dates.should.have.length(4);
  });

  it("should return the active recurrent block (the latest created)", () => {
    let cp = new CustodyPlan();
    const uid = 1;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur.setDate(uid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.setDate(uid, moment("20170329"));
    recur.setDate(uid, moment("20170402"));
    recur.label = "1";

    cp.addBlock(recur);

    let recur2 = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur2.setDate(uid, moment("20170320"));
    recur2.setDate(uid, moment("20170324"));
    recur2.setDate(uid, moment("20170329"));
    recur2.setDate(uid, moment("20170402"));
    recur2.label = "2";

    cp.addBlock(recur2);

    let recur3 = cp.createRecurringBlock(
      uid,
      2,
      moment(start).add(-10, "day"),
      moment(start).add(-3, "day")
    );
    recur3.setDate(uid, moment("20170320"));
    recur3.setDate(uid, moment("20170324"));
    recur3.setDate(uid, moment("20170329"));
    recur3.setDate(uid, moment("20170402"));
    recur3.label = "3";

    cp.addBlock(recur3);

    let recur4 = cp.createRecurringBlock(
      uid,
      2,
      moment("20180101"),
      moment("20190101")
    );
    recur4.setDate(uid, moment("20180320"));
    recur4.setDate(uid, moment("20180324"));
    recur4.setDate(uid, moment("20180329"));
    recur4.setDate(uid, moment("20180402"));
    recur4.label = "4";

    cp.addBlock(recur4);

    const block = cp.getActiveBlock(start);

    assert(block.label, "2", "Should be 2!");
  });

  it("should return true if any custody blocks", () => {
    let cal = new Calendar();
    let cp = new CustodyPlan();
    const uid = 1;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur.setDate(uid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.setDate(uid, moment("20170329"));
    recur.setDate(uid, moment("20170402"));

    assert(cal.hasAnyCustodyBlocks === false, "should be false");

    cal.addCustodyPlan(cp);

    assert(cal.hasAnyCustodyBlocks === false, "should be false");

    cp.addBlock(recur);

    assert(cal.hasAnyCustodyBlocks === true, "should be true");
  });

  it("should return events in date range", () => {
    // create 3 events
    let cal = new Calendar();
    cal.addEvent({
      dtStart: moment("20160315").toDate(),
      topic: {
        title: "Event01"
      }
    });
    cal.addEvent({
      dtStart: moment("20160316").toDate(),
      dtEnd: moment("20160317").toDate(),
      topic: {
        title: "Event02"
      }
    });
    cal.addEvent({
      dtStart: moment("20160318").toDate(),
      dtEnd: moment("20160322").toDate(),
      topic: {
        title: "Event03"
      }
    });

    const events = cal.getEvents(
      null,
      moment("20160315").toDate(),
      moment("20160317").toDate()
    );
    assert(events.length === 2, "Should be 2");
    assert(events[0].topic.title === "Event01");
  });

  it("should create a recurring block and initialize weeks", () => {
    let cp = new CustodyPlan();
    const uid = 1;
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    assert(typeof recur.data.weeks[0] === "object");
    assert(typeof recur.data.weeks[1] === "object");
  });

  it("should returns users who has custody in a period", () => {
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1",
      uid2 = "user2";
    const start = moment("20170320");

    let recur = cp.createRecurringBlock(uid, 2, start, moment("20170501"));
    recur.setDate(uid, moment("20170320"));
    recur.setDate(uid, moment("20170324"));
    recur.setDate(uid2, moment("20170324"));
    recur.setDate(uid2, moment("20170329"));
    recur.setDate(uid2, moment("20170402"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    let users = cp.getWhoHasCustody(
      moment("20170320").toDate(),
      moment("20170329").toDate()
    );

    users.should.include.something.that.equals(uid);
    users.should.include.something.that.equals(uid2);

    users = cp.getWhoHasCustody(
      moment("20170320").toDate(),
      moment("20170323").toDate()
    );

    users.should.include.something.that.equals(uid);
    users.should.not.include.something.that.equals(uid2);

    users = cp.getWhoHasCustody(
      moment("20170325").toDate(),
      moment("20170401").toDate()
    );

    users.should.not.include.something.that.equals("user1");
    users.should.include.something.that.equals("user2");

    users = cp.getWhoHasCustody(
      moment("20170330").toDate(),
      moment("20170401").toDate()
    );

    users.should.not.include.something.that.equals("user1");
    users.should.not.include.something.that.equals("user2");
  });

  it("should serialize RRuleSet correctly", () => {
    const rruleSet = new RRuleSet();

    // Add a rrule to rruleSet
    rruleSet.rrule(
      new RRule({
        freq: RRule.MONTHLY,
        count: 5,
        dtstart: new Date(2012, 1, 1, 10, 30)
      })
    );

    const event = {
      rruleset: rruleSet
    };

    let cal = new Calendar();
    cal.addEvent(event);

    let obj = cal.toObject();

    cal = Calendar.fromObject(obj);

    const rr = cal.data.events[0].rruleset;

    assert(rr.all().length === 5, "should contain 5 dates");
  });

  it("should return all-day", () => {
    const event = {
      allDay: true,
      dtStart: moment().startOf("day")
    };

    const str = Calendar.PPEventTimeRange(event);
    expect(str).to.have.string("All day");
  });

  it("should repeat every week", () => {
    const rruleSet = new RRuleSet();

    // Add a rrule to rruleSet
    rruleSet.rrule(
      new RRule({
        freq: RRule.WEEKLY,
        interval: 1,
        until: moment("20100301").toDate(),
        dtstart: moment("20100101").toDate()
      })
    );

    const event = {
      rruleset: rruleSet
    };

    let cal = new Calendar();
    cal.addEvent(event);

    let events = cal.getEvents(
      null,
      moment("20100101").toDate(),
      moment("20100301").toDate()
    );
    assert(events.length === 1, "should contain 1 events");

    events = cal.getEvents(
      null,
      moment("20100201").toDate(),
      moment("20100301").toDate()
    );
    assert(events.length === 1, "should contain 1 events");

    events = cal.getEvents(
      null,
      moment("20100215").toDate(),
      moment("20100301").toDate()
    );
    assert(events.length === 1, "should contain 1 events");

    events = cal.getEvents(
      null,
      moment("20100401").toDate(),
      moment("20100501").toDate()
    );
    assert(events.length === 0, "should contain 0 events");
  });

  it("should cap recurring block by max and min dates", () => {
    // 7/7 plan
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1";
    const start = moment("20171204");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20171206"),
      moment("20171223")
    );
    recur.setDate(uid, moment("20171204"));
    recur.setDate(uid, moment("20171205"));
    recur.setDate(uid, moment("20171206"));
    recur.setDate(uid, moment("20171207"));
    recur.setDate(uid, moment("20171208"));
    recur.setDate(uid, moment("20171209"));
    recur.setDate(uid, moment("20171210"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    const dates = cal.getCustodyDatesForUser(
      uid,
      moment("20171201"),
      moment("20171230")
    );
    dates.should.have.length(11);
    dates.should.include.something.that.deep.equals(
      moment("20171206").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171223").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171205").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171224").toDate()
    );
  });

  it("should cap recurring block by min dates", () => {
    // 7/7 plan
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1";
    const start = moment("20171204");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20171206")
    );
    recur.setDate(uid, moment("20171204"));
    recur.setDate(uid, moment("20171205"));
    recur.setDate(uid, moment("20171206"));
    recur.setDate(uid, moment("20171207"));
    recur.setDate(uid, moment("20171208"));
    recur.setDate(uid, moment("20171209"));
    recur.setDate(uid, moment("20171210"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    const dates = cal.getCustodyDatesForUser(
      uid,
      moment("20171201"),
      moment("20171230")
    );
    dates.should.have.length(12);
    dates.should.include.something.that.deep.equals(
      moment("20171206").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171223").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171205").toDate()
    );
  });

  it("should return dates when year is shifting", () => {
    // 7/7 plan
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1";
    const start = moment("20171204");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20171206")
    );
    recur.setDate(uid, moment("20171204"));
    recur.setDate(uid, moment("20171205"));
    recur.setDate(uid, moment("20171206"));
    recur.setDate(uid, moment("20171207"));
    recur.setDate(uid, moment("20171208"));
    recur.setDate(uid, moment("20171209"));
    recur.setDate(uid, moment("20171210"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    const dates = cal.getCustodyDatesForUser(
      uid,
      moment("20170101"),
      moment("20180131")
    );
    dates.should.have.length(29);
    dates.should.include.something.that.deep.equals(
      moment("20171206").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20180101").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20180102").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20180103").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20180109").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171205").toDate()
    );
  });

  it("should return dates when year is shifting when min date is 1 month later", () => {
    // 7/7 plan
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1";
    const start = moment("20171204");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20180101")
    );
    recur.setDate(uid, moment("20171204"));
    recur.setDate(uid, moment("20171205"));
    recur.setDate(uid, moment("20171206"));
    recur.setDate(uid, moment("20171207"));
    recur.setDate(uid, moment("20171208"));
    recur.setDate(uid, moment("20171209"));
    recur.setDate(uid, moment("20171210"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    const dates = cal.getCustodyDatesForUser(
      uid,
      moment("20170101"),
      moment("20180131")
    );
    dates.should.have.length(17);
    dates.should.not.include.something.that.deep.equals(
      moment("20171206").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20180101").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20180102").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20180103").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20180109").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171205").toDate()
    );
  });

  it("should return correct dates when start and min date is changing", () => {
    // 7/7 plan
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1";
    const start = moment("20171215").startOf("week");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20171215")
    );
    recur.setDate(uid, moment("20171211"));
    recur.setDate(uid, moment("20171212"));
    recur.setDate(uid, moment("20171213"));
    recur.setDate(uid, moment("20171214"));
    recur.setDate(uid, moment("20171215"));
    recur.setDate(uid, moment("20171216"));
    recur.setDate(uid, moment("20171217"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    let dates = cal.getCustodyDatesForUser(
      uid,
      moment("20171201"),
      moment("20180131")
    );
    dates.should.have.length(24);

    const newMinDate = moment("20171206");
    const newStart = moment(newMinDate).startOf("week");
    recur.data.minimumDate = newMinDate.toDate();
    recur.data.startDate = newStart.toDate();

    dates = cal.getCustodyDatesForUser(
      uid,
      moment("20171201"),
      moment("20180131")
    );
    dates.should.include.something.that.deep.equals(
      moment("20171210").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171211").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171212").toDate()
    );
  });

  it("fill out plan with userId custody", () => {
    // 7/7 plan
    let cal = new Calendar();
    let cp = new CustodyPlan(1);
    const uid = "user1";
    const start = moment("20171215").startOf("week");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20171215")
    );
    recur.setDate(uid, moment("20171211"));
    recur.setDate(uid, moment("20171212"));
    recur.setDate(uid, moment("20171213"));
    recur.setDate(uid, moment("20171214"));
    recur.setDate(uid, moment("20171215"));
    recur.setDate(uid, moment("20171216"));
    recur.setDate(uid, moment("20171217"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    // fill out the rest with userId2
    recur.fillEmptyDates("userId2");

    const dates = cal.getCustodyDatesForUser(
      "userId2",
      moment("20171201"),
      moment("20180131")
    );
    dates.should.include.something.that.deep.equals(
      moment("20171218").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171219").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171220").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171221").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171222").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171223").toDate()
    );
    dates.should.include.something.that.deep.equals(
      moment("20171224").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171211").toDate()
    );
    dates.should.not.include.something.that.deep.equals(
      moment("20171225").toDate()
    );
  });

  it("should recur every other week", () => {
    const rruleSet = new RRuleSet();

    // Add a rrule to rruleSet
    rruleSet.rrule(
      new RRule({
        freq: RRule.MONTHLY,
        dtstart: new Date(2012, 1, 1, 10, 30)
      })
    );

    const event1 = {
      dtStart: moment("20180101").toDate(),
      rruleset: rruleSet
    };
    const event2 = {
      dtStart: moment("20180101").toDate(),
      rruleset: rruleSet
    };

    const event3 = {
      dtStart: moment("20171215").toDate(),
      rruleset: rruleSet
    };

    let cal = new Calendar();
    cal.addEvent(event1);
    cal.addEvent(event2);
    cal.addEvent(event3);

    const events = cal.getEvents(
      null,
      moment("20180101").toDate(),
      moment("20180201").toDate()
    );

    events.should.have.length(3);
  });

  it("should return the current/or latest custody period", () => {
    // 7/7 plan
    const uid = "user1";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const start = moment("20171215").startOf("week");

    let recur = cp.createRecurringBlock(
      uid,
      2,
      start,
      null,
      moment("20171215")
    );
    recur.setDate(uid, moment("20171211"));
    recur.setDate(uid, moment("20171212"));
    recur.setDate(uid, moment("20171213"));
    recur.setDate(uid, moment("20171214"));
    recur.setDate(uid, moment("20171215"));
    recur.setDate(uid, moment("20171216"));
    recur.setDate(uid, moment("20171217"));

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    const latestBlock = cal.getLatestActiveBlock(uid, childId);
    assert(latestBlock !== null, "should not be null");
    const diff = moment(latestBlock.to).diff(latestBlock.from, "days");
    assert(diff !== 0, "should not be 0");
  });

  it("should return today as well", () => {
    const uid = "user1";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const start = moment().add(-2, "days");

    let recur = cp.createRecurringBlock(uid, 2, start);
    recur.setDate(
      uid,
      moment()
        .add(-2, "day")
        .startOf("day")
    );
    recur.setDate(
      uid,
      moment()
        .add(-1, "day")
        .startOf("day")
    );
    recur.setDate(uid, moment().startOf("day"));
    recur.setDate(
      uid,
      moment()
        .add(1, "day")
        .startOf("day")
    );

    cp.addBlock(recur);
    cal.addCustodyPlan(cp);
    assert(cal.hasAnyCustodyBlocks === true, "should be true");

    const latestBlock = cal.getLatestActiveBlock(uid, childId);
    assert(latestBlock !== null, "should not be null");
    const diff = moment(latestBlock.to).diff(latestBlock.from, "days");
    assert(diff === 1, "should be 6");
  });

  it("should return the active custodyplan", () => {
    const uid1 = "user1";
    const uid2 = "user2";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const start = moment().startOf("day");

    let recur1 = cp.createRecurringBlock(uid1, 2, start);
    cp.addBlock(recur1);
    for (let i = 0; i < 7; i++)
      recur1.setDate(
        uid1,
        moment(start)
          .add(i, "day")
          .startOf("day")
      );
    recur1.fillEmptyDates(uid2);

    cal.addCustodyPlan(cp);

    // insert swap
    const swap = cp.createRangeBlock();
    swap.setCustodyForDate(uid2, moment(start).add(1, "day"));
    swap.setCustodyForDate(uid2, moment(start).add(3, "day"));
    swap.fillEmptyDates(uid1);

    const rangeDates1 = swap.range(
      uid1,
      moment(start),
      moment(start).add(13, "day")
    );
    assert(rangeDates1.length === 1, "should be 1");
    const rangeDates2 = swap.range(
      uid2,
      moment(start),
      moment(start).add(13, "day")
    );
    assert(rangeDates2.length === 2, "should be 2");

    cp.addBlock(swap);

    const dates = cal.getCustodyDatesForUser(
      uid1,
      start,
      moment(start)
        .add(13, "day")
        .endOf("day")
    );
    assert(dates.length === 7, "should be 7 days");
    const dates2 = cal.getCustodyDatesForUser(
      uid2,
      start,
      moment(start)
        .add(13, "day")
        .endOf("day")
    );
    assert(dates2.length === 7, "should be 7 days");
  });

  it("should split multi-day events into multiple days", () => {
    const event1 = {
      dtStart: moment("2018-01-02 12:00:00").toDate(),
      dtEnd: moment("2018-01-05 17:00:00").toDate()
    };

    const event2 = {
      dtStart: moment("2018-01-25 11:00:00").toDate(),
      dtEnd: moment("2018-01-25 16:00:00").toDate()
    };

    const cal = new Calendar();
    cal.addEvent(event1);
    cal.addEvent(event2);

    const events = cal.getEventsPerDay(
      null,
      moment("20180101").toDate(),
      moment("20180201").toDate()
    );

    events.should.have.length(5);

    events[0].dtStart.getHours().should.equal(12);
    events[1].dtStart.getHours().should.equal(0);
    events[2].dtEnd.getHours().should.equal(23);
    events[3].dtEnd.getHours().should.equal(17);
  });

  xit("should split multi-day events into multiple weeks", () => {
    const event1 = {
      dtStart: moment("2018-01-02 12:00:00").toDate(),
      dtEnd: moment("2018-01-05 17:00:00").toDate()
    };

    const event2 = {
      dtStart: moment("2018-01-25 11:00:00").toDate(),
      dtEnd: moment("2018-01-31 16:00:00").toDate()
    };

    const cal = new Calendar();
    cal.addEvent(event1);
    cal.addEvent(event2);

    const events = cal.getEventsPerDay(
      null,
      moment("20180101").toDate(),
      moment("20180201").toDate(),
      null,
      "week"
    );

    events.should.have.length(3);

    events[0].dtStart.getHours().should.equal(12);
    events[1].dtStart.getHours().should.equal(11);
    events[1].dtEnd.getHours().should.equal(0);
    events[2].dtEnd.getHours().should.equal(16);
  });

  it("should cap events by time window", () => {
    const event1 = {
      dtStart: moment("2019-05-28 11:00:00").toDate(),
      dtEnd: moment("2019-06-05 12:00:00").toDate()
    };

    const cal = new Calendar();
    cal.addEvent(event1);

    const events1 = cal.getEventsInInterval(
      null,
      moment("20190527").toDate(),
      moment("20190602").toDate()
    );
    events1.should.have.length(1);

    events1[0].dtStart.getHours().should.equal(11);
    events1[0].dtEnd.getHours().should.equal(0);

    const events2 = cal.getEventsInInterval(
      null,
      moment("20190601").toDate(),
      moment("20190607").toDate()
    );
    events2.should.have.length(1);

    events2[0].dtStart.getHours().should.equal(0);
    events2[0].dtEnd.getHours().should.equal(12);
  });

  it("should respect drop off and pick up time", () => {
    const uid1 = "user1";
    const uid2 = "user2";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const block1 = {
      days: 7,
      startTime: 8,
      endTime: 16,
      children: [childId],
      users: [uid1],
      id: 1
    };
    const block2 = {
      days: 4,
      startTime: 16,
      endTime: 8,
      children: [childId],
      users: [uid2],
      prevBlock: block1,
      id: 2
    };
    const blocks = [block1, block2];

    const cb = cp.createCustodyBlock({
      userId: uid1,
      blocks,
      childrenIds: [childId],
      startDate: moment("20190101").toDate(),
      repeat: true,
      title: "Vinter"
    });

    cp.addBlock(cb);

    // uid1
    let users = cp.getWhoHasCustody(
      moment("20190101").toDate(),
      moment("20190103").toDate()
    );
    users.should.have.length(1);
    users.should.contain(uid1);

    users = cp.getWhoHasCustody(
      moment("20190109").toDate(),
      moment("20190110").toDate()
    );
    users.should.have.length(1);
    users.should.contain(uid2);

    users = cp.getWhoHasCustody(
      moment("20190108").toDate(),
      moment("20190109").toDate()
    );
    users.should.have.length(2);
    users.should.contain(uid2);
    users.should.contain(uid1);

    // uid
    let users1 = cp.getWhoHasCustody(
      moment("2019-01-01 07:00").toDate(),
      moment("2019-01-01 07:10").toDate()
    );
    users1.should.have.length(0);
  });

  it("should follow recurrences", () => {
    const uid1 = "user1";
    const uid2 = "user2";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const block1 = {
      days: 7,
      startTime: 8,
      endTime: 16,
      children: [childId],
      users: [uid1],
      id: 1
    };
    const block2 = {
      days: 4,
      startTime: 16,
      endTime: 8,
      children: [childId],
      users: [uid2],
      prevBlock: block1,
      id: 2
    };
    const blocks = [block1, block2];

    const cb = cp.createCustodyBlock({
      userId: uid1,
      blocks,
      childrenIds: [childId],
      startDate: moment("20190101").toDate(),
      repeat: true,
      title: "Vinter"
    });

    cp.addBlock(cb);

    cb._blockStartInMinutes(block2).should.equal(7 * 24 * 60 + 16 * 60);

    // should be in the *next* recurrence
    let users1 = cp.getWhoHasCustody(
      moment("2019-01-13 08:01").toDate(),
      moment("2019-01-13 08:10").toDate()
    );
    users1.should.have.length(1);
    users1.should.contain(uid1);

    users1 = cp.getWhoHasCustody(
      moment("2019-01-08 15:16").toDate(),
      moment("2019-01-08 16:10").toDate()
    );
    users1.should.have.length(2);
    users1.should.contain(uid1);
    users1.should.contain(uid2);

    users1 = cp.getWhoHasCustody(
      moment("2019-01-12 7:01").toDate(),
      moment("2019-01-12 8:01").toDate()
    );
    users1.should.have.length(2);
    users1.should.contain(uid1);
    users1.should.contain(uid2);
  });

  it("should return list of dates", () => {
    const uid1 = "user1";
    const uid2 = "user2";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const block1 = {
      days: 1,
      startTime: 8,
      endTime: 16,
      children: [childId],
      users: [uid1]
    };
    const block2 = {
      days: 7,
      startTime: 16,
      endTime: 8,
      children: [childId],
      users: [uid2],
      prevBlock: block1
    };
    const blocks = [block1, block2];

    const cb = cp.createCustodyBlock({
      userId: uid1,
      blocks,
      childrenIds: [childId],
      startDate: moment("20190101").toDate(),
      repeat: true,
      title: "Vinter"
    });

    cp.addBlock(cb);

    const dates = cp.getCustodyDatesForUser(
      uid1,
      moment("20180101").toDate(),
      moment("20190201").toDate()
    );

    dates.should.have.length(8);
    dates[0].getDate().should.equal(1);
    dates[0].getFullYear().should.equal(2019);
    dates[1].getDate().should.equal(2);
    dates[2].getDate().should.equal(9);
    dates[3].getDate().should.equal(10);
  });

  it("should not repeat", () => {
    const uid1 = "user1";
    const uid2 = "user2";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const block1 = {
      days: 1,
      startTime: 8,
      endTime: 0,
      children: [childId],
      users: [uid1]
    };
    const block2 = {
      days: 1,
      startTime: 16,
      endTime: 0,
      children: [childId],
      users: [uid2],
      prevBlock: block1
    };
    const blocks = [block1, block2];

    const cb = cp.createCustodyBlock({
      userId: uid1,
      blocks,
      childrenIds: [childId],
      startDate: moment("20190101").toDate(),
      repeat: false,
      title: "2 dage"
    });

    cp.addBlock(cb);

    let users = cp.getWhoHasCustody(
      moment("20190104")
        .startOf("day")
        .toDate(),
      moment("20190104")
        .endOf("day")
        .toDate()
    );
    users.should.not.contain(uid1);
    users.should.not.contain(uid2);
  });

  xit("should handle overlaps", () => {
    const uid1 = "user1";
    const uid2 = "user2";
    const childId = "childId";
    let cal = new Calendar();
    let cp = new CustodyPlan(childId);

    const block1 = {
      days: 6,
      startTime: 8,
      endTime: 0,
      children: [childId],
      users: [uid1]
    };
    const block2 = {
      days: 7,
      startTime: 16,
      endTime: 0,
      children: [childId],
      users: [uid2],
      prevBlock: block1
    };
    const blocks = [block1, block2];

    const cb = cp.createCustodyBlock({
      userId: uid1,
      blocks,
      childrenIds: [childId],
      startDate: moment("20190101").toDate(),
      repeat: false,
      title: "13 dage"
    });

    cp.addBlock(cb);

    const block3 = {
      days: 2,
      startTime: 8,
      endTime: 0,
      children: [childId],
      users: [uid1]
    };
    const block4 = {
      days: 3,
      startTime: 16,
      endTime: 0,
      children: [childId],
      users: [uid2],
      prevBlock: block1
    };
    const blocks2 = [block3, block4];

    const cb2 = cp.createCustodyBlock({
      userId: uid1,
      blocks: blocks2,
      childrenIds: [childId],
      startDate: moment("20190301").toDate(),
      repeat: true,
      title: "Gentagelse fra 1.feb"
    });

    cp.addBlock(cb2);

    let users = cp.getWhoHasCustody(
      moment("20190301")
        .startOf("day")
        .toDate(),
      moment("20190302")
        .endOf("day")
        .toDate()
    );
    users.should.contain(uid1);
    users.should.not.contain(uid2);

    users = cp.getWhoHasCustody(
      moment("20190107")
        .startOf("day")
        .toDate(),
      moment("20190108")
        .endOf("day")
        .toDate()
    );
    users.should.contain(uid2);
    users.should.not.contain(uid1);
  });

  it("should show correct dates", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      createdAt: "2019-11-07T16:18:45.937Z",
      totalDays: 6,
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          prevBlock: "c73a6af0-0174-11ea-a23a-37caf806e38f",
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 0,
          startTime: 0,
          days: 4,
          users: [uid2],
          id: "c7b1a480-0179-11ea-987f-abd1cdd7fe9f"
        }
      ],
      repeat: false,
      title: "Test",
      minimumDate: "2019-11-07T03:00:11.208Z",
      endDate: "2019-11-13T23:00:11.208Z",
      startDate: "2019-11-07T03:00:11.208Z",
      type: 3,
      childrenIds: ["5dc3009c8724333f637d4072", "5dc3009c8724333f637d4073"],
      sharedId: "45f1e4e0-017a-11ea-987f-abd1cdd7fe9f"
    };

    const rb = CustodyBlock.fromObject(block);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    let users = cp.getWhoHasCustody(
      moment("20191110")
        .startOf("day")
        .toDate(),
      moment("20191111")
        .endOf("day")
        .toDate()
    );
    users.should.contain(uid2);
    users.should.not.contain(uid1);
  });

  it("should return date interval (on the same day)", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      createdAt: "2019-11-07T16:18:45.937Z",
      totalDays: 6,
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 20,
          startTime: 3,
          days: 0,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ],
      repeat: false,
      title: "Test",
      minimumDate: "2019-11-07T03:00:00.000Z",
      startDate: "2019-11-07T03:00:00.000Z",
      type: 3,
      childrenIds: ["5dc3009c8724333f637d4072", "5dc3009c8724333f637d4073"],
      sharedId: "45f1e4e0-017a-11ea-987f-abd1cdd7fe9f"
    };

    const rb = CustodyBlock.fromObject(block);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    const intervals = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191107")
        .startOf("day")
        .toDate(),
      moment("20191107")
        .endOf("day")
        .toDate()
    );
    // test
    intervals.should.have.length(1);
    intervals[0][0].getHours().should.equal(3);
    intervals[0][1].getHours().should.equal(20);
  });

  it("should return dates interval (spanning multiple days)", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      createdAt: "2019-11-07T16:18:45.937Z",
      totalDays: 6,
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 18,
          startTime: 5,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ],
      repeat: false,
      title: "Test",
      minimumDate: "2019-11-07T03:00:00.000Z",
      startDate: "2019-11-07T03:00:00.000Z",
      type: 3,
      childrenIds: ["5dc3009c8724333f637d4072", "5dc3009c8724333f637d4073"],
      sharedId: "45f1e4e0-017a-11ea-987f-abd1cdd7fe9f"
    };

    const rb = CustodyBlock.fromObject(block);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    let intervals = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191107").toDate(),
      moment("20191107")
        .endOf("day")
        .toDate()
    );
    // should return 4 - 2359
    intervals.should.have.length(1);
    intervals[0][0].getHours().should.equal(5);
    intervals[0][1].getHours().should.equal(23);

    intervals = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191108").toDate(),
      moment("20191108")
        .endOf("day")
        .toDate()
    );
    // should return 4 - 2359
    intervals.should.have.length(1);
    intervals[0][0].getHours().should.equal(0);
    intervals[0][1].getHours().should.equal(23);

    intervals = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191109").toDate(),
      moment("20191109")
        .endOf("day")
        .toDate()
    );
    // should return 4 - 2359
    intervals.should.have.length(1);
    intervals[0][0].getHours().should.equal(0);
    intervals[0][1].getHours().should.equal(18);
  });

  it("should return dates interval recurrently", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      createdAt: "2019-11-07T16:18:45.937Z",
      totalDays: 4,
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 8,
          days: 2,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38y",
          prevBlock: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ],
      repeat: true,
      title: "Test",
      minimumDate: "2019-11-07T03:00:00.000Z",
      startDate: "2019-11-07T03:00:00.000Z",
      type: 3,
      childrenIds: ["5dc3009c8724333f637d4072", "5dc3009c8724333f637d4073"],
      sharedId: "45f1e4e0-017a-11ea-987f-abd1cdd7fe9f"
    };

    const rb = CustodyBlock.fromObject(block);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    let intervals = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191107").toDate(),
      moment("20191127")
        .endOf("day")
        .toDate()
    );
    // should return 4 - 2359
    intervals.should.have.length(16);
    intervals[0][0].getHours().should.equal(4);
    intervals[0][1].getHours().should.equal(23);

    intervals[1][0].getDate().should.equal(8);
    intervals[1][1].getDate().should.equal(8);

    intervals[2][0].getHours().should.equal(0);
    intervals[2][1].getHours().should.equal(19);

    intervals[2][0].getDate().should.equal(9);
    intervals[2][1].getDate().should.equal(9);
  });

  it("should return dates in range", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      createdAt: "2019-11-07T16:18:45.937Z",
      totalDays: 4,
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 19,
          days: 2,
          users: [uid2],
          prevBlock: "c73a6af0-0174-11ea-a23a-37caf806e38f",
          id: "c73a6af0-0174-11ea-a23a-37caf806e386"
        }
      ],
      repeat: true,
      title: "Test",
      minimumDate: "2019-01-01T03:00:00.000Z",
      startDate: "2019-01-01T03:00:00.000Z",
      type: 3,
      childrenIds: ["5dc3009c8724333f637d4072", "5dc3009c8724333f637d4073"],
      sharedId: "45f1e4e0-017a-11ea-987f-abd1cdd7fe9f"
    };

    const rb = CustodyBlock.fromObject(block);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    let intervals = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20190107").toDate(),
      moment("20190227")
        .endOf("day")
        .toDate()
    );
  });

  it("should return correct length", () => {
    const uid1 = "user1";
    const uid2 = "666";

    let block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 0,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ]
    };

    let rb = CustodyBlock.fromObject(block);

    rb._lengthInMinutes().should.equal((19 - 4) * 60);

    block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ]
    };

    rb = CustodyBlock.fromObject(block);

    rb._lengthInMinutes().should.equal(63 * 60);

    block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 12,
          startTime: 12,
          days: 2,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ]
    };

    rb = CustodyBlock.fromObject(block);

    rb._lengthInMinutes().should.equal(104 * 60);
  });

  it("should return correct start time in minutes of blocks", () => {
    const uid1 = "user1";
    const uid2 = "666";

    let block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 0,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ]
    };

    let rb = CustodyBlock.fromObject(block);

    rb._blockStartInMinutes(block.blocks[0]).should.equal(4 * 60);

    block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 12,
          startTime: 12,
          days: 2,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38d"
        }
      ]
    };

    rb = CustodyBlock.fromObject(block);

    rb._blockStartInMinutes(block.blocks[1]).should.equal(60 * 60);

    const block1 = {
      days: 7,
      startTime: 8,
      endTime: 16,

      users: [uid1],
      id: 1
    };
    const block2 = {
      days: 4,
      startTime: 16,
      endTime: 8,

      users: [uid2],
      prevBlock: block1,
      id: 2
    };
    const blocks = [block1, block2];

    const childId = "childId";
    let cp = new CustodyPlan(childId);

    let cb = cp.createCustodyBlock({
      userId: uid1,
      blocks,
      startDate: moment("20190101").toDate(),
      repeat: true,
      title: "Vinter"
    });

    cb._blockStartInMinutes(block1).should.equal(8 * 60);

    cb._blockStartInMinutes(block2).should.equal(184 * 60);
  });

  it("should return correct length of blocks in minutes", () => {
    const uid1 = "user1";
    const uid2 = "666";

    let block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 0,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        }
      ]
    };

    let rb = CustodyBlock.fromObject(block);

    rb._blockLengthInMinutes(block.blocks[0]).should.equal(15 * 60);

    block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 19,
          startTime: 4,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 12,
          startTime: 12,
          days: 2,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38d"
        }
      ]
    };

    rb = CustodyBlock.fromObject(block);

    rb._blockLengthInMinutes(block.blocks[0]).should.equal(63 * 60);
    rb._blockLengthInMinutes(block.blocks[1]).should.equal(48 * 60);
  });

  it("should do a 7/7 schedule correct", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 7,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 7,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38d"
        }
      ],
      repeat: true,
      startDate: moment("20191111").toDate()
    };

    const rb = CustodyBlock.fromObject(block);

    rb._blockLengthInMinutes(block.blocks[0]).should.equal(
      rb._blockLengthInMinutes(block.blocks[1])
    );
    rb._totalDaysInBlocks(block.blocks).should.equal(14);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    const dates = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191125").toDate(),
      moment("20191125").toDate()
    );

    dates[0][0].getHours().should.equal(16);
    dates[0][1].getHours().should.equal(23);
  });

  it("should respect endDate", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const block = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 7,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 7,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38d"
        }
      ],
      repeat: true,
      startDate: moment("20191111").toDate(),
      endDate: moment("20191212").toDate()
    };

    const rb = CustodyBlock.fromObject(block);

    rb._blockLengthInMinutes(block.blocks[0]).should.equal(
      rb._blockLengthInMinutes(block.blocks[1])
    );
    rb._totalDaysInBlocks(block.blocks).should.equal(14);

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);

    const dates = cp.getCustodyDateIntervalsPerDayForUser(
      uid1,
      moment("20191213").toDate(),
      moment("20301225").toDate()
    );

    dates.should.be.empty
  })

  it("should be in the right order - repeat is prioritized", () => {
    const uid1 = "user1";
    const uid2 = "666";

    const template = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 7,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 7,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38d"
        }
      ],
      repeat: true,
      startDate: moment("20191220").toDate(),
      endDate: moment("20201212").toDate(),
      createdAt: moment("20191210").toDate()
    };
    const vacay = {
      blocks: [
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 2,
          users: [uid1],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38f"
        },
        {
          dropOffLocation: "",
          pickupLocation: "",
          endTime: 8,
          startTime: 16,
          days: 2,
          users: [uid2],
          id: "c73a6af0-0174-11ea-a23a-37caf806e38d"
        }
      ],
      repeat: false,
      startDate: moment("20191224").toDate(),
      createdAt: moment("20191023").toDate()
    };

    const rb = CustodyBlock.fromObject(template);
    rb.createdAt.getDate().should.equal(10)
    rb.createdAt.getMonth().should.equal(11)

    const rb1 = CustodyBlock.fromObject(vacay);
    rb1.createdAt.getDate().should.equal(23)
    rb1.createdAt.getMonth().should.equal(9)

    const childId = "childId";
    let cp = new CustodyPlan(childId);
    cp.addBlock(rb);
    cp.addBlock(rb1);

    const dates = cp.getCustodyDateIntervalsPerDayForUser(uid1,moment("20191222").toDate(), moment("20191226").toDate())

    dates.length.should.equal(2)


  })
});
