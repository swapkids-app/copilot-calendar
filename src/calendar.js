import Moment from 'moment/min/moment-with-locales';
import { extendMoment } from 'moment-range';
import _ from 'lodash';
import { rrulestr } from 'rrule';
import CustodyPlan from './custodyPlan';
import transformProps from 'transform-props';

const moment = extendMoment(Moment);

export default class Calendar {
  data = {
    custodyPlans: [],
    events: [],
  };

  addCustodyPlan = (plan) => {
    const other = this.custodyPlanForChild(plan.childId);
    if (!other) {
      this.data.custodyPlans.push(plan);
    } else {
      // throw
    }
  };

  get custodyPlans() {
    return this.data.custodyPlans;
  }

  get hasAnyCustodyBlocks() {
    return this.custodyPlans.some(p => p.blocks.length);
  }

  get events() {
    return this.data.events;
  }

  get allBlocks() {
    return _.flatten(this.data.custodyPlans.map(p => p.blocks));
  }

  addEvent = (event) => {
    this.data.events.push(event);
  };

  /**
   * Returns all events in date range [fromDate, toDate[
   * Provide your custom events or use events in the calendar (by passing null)
   * @param childId
   * @param fromDate
   * @param toDate
   * @param events
   * @return {Array.<*>}
   */
  getEvents = (childId, fromDate, toDate, events = null) => {
    const mFromDate = moment(fromDate);
    const mToDate = moment(toDate);
    const range = moment.range(mFromDate, mToDate);
    const es = events || this.data.events;
    if (es) {
      return es.filter((event) => {
        if (!event.rruleset && event.dtEnd < fromDate) {
          return false;
        }
        if (event.dtStart > toDate) {
          return false;
        }
        if (childId && !event.children.includes(childId)) {
          return false;
        }
        if (event.rruleset) {
          const dates = event.rruleset.between(
            mFromDate.toDate(),
            mToDate.toDate(),
            true,
          );
          return !!dates.length;
        }
        const { dtStart, dtEnd } = event;
        const eventRange = event.dtEnd
          ? moment.range(dtStart, dtEnd)
          : moment.range(dtStart, moment(dtStart).endOf('day'));
        return range.overlaps(eventRange);
      });
    }

    return [];
  };

  /**
   * Returns all events in date range [fromDate, toDate[ offset'ed with fromDate and toDate
   * Provide your custom events or use events in the calendar (by passing null)
   * @param fromDate
   * @param toDate
   * @param events
   * @return {Array.<*>}
   */
  getEventsPerDay = (
    childId,
    fromDate,
    toDate,
    events = null,
    unitOfTime = 'day',
  ) => {
    const mFromDate = moment(fromDate).startOf(unitOfTime);
    const mToDate = moment(toDate).endOf(unitOfTime);
    const range = moment.range(mFromDate, mToDate);
    const es = events || this.data.events;
    if (es) {
      return es.reduce((result, event) => {
        const dtStart = event.allDay
          ? moment(event.dtStart)
              .startOf('day')
              .toDate()
          : event.dtStart;
        const dtEnd = event.allDay
          ? moment(event.dtEnd)
              .endOf('day')
              .toDate()
          : event.dtEnd;

        if (!event.rruleset && dtEnd < fromDate) {
          return result;
        }
        if (childId && !event.children.includes(childId)) {
          return result;
        }
        if (event.rruleset) {
          const dates = event.rruleset.between(
            mFromDate.toDate(),
            mToDate.toDate(),
            true,
          );
          dates.forEach((date) => {
            result.push({
              ...event,
              dtStart: new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate(),
                dtStart.getHours(),
                dtStart.getMinutes(),
              ),
              dtEnd: new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate(),
                dtEnd.getHours(),
                dtEnd.getMinutes(),
              ),
            });
          });
          return result;
        }
        const eventRange = dtEnd
          ? moment.range(
              moment(dtStart).startOf(unitOfTime),
              moment(dtEnd).endOf(unitOfTime),
            )
          : moment.range(
              moment(dtStart).startOf(unitOfTime),
              moment(dtStart).endOf(unitOfTime),
            );

        const overlap = range.intersect(eventRange);
        if (overlap) {
          for (const day of overlap.by(unitOfTime)) {
            const start =
              dtStart > day.startOf(unitOfTime).toDate()
                ? dtStart
                : day.toDate();
            const end =
              dtEnd < day.endOf(unitOfTime).toDate()
                ? dtEnd
                : day.endOf(unitOfTime).toDate();
            const newEvent = {
              ...event,
              origDtStart: event.dtStart,
              origDtEnd: event.dtEnd,
              dtStart: start,
              dtEnd: end,
            };
            result.push(newEvent);
          }
        }

        return result;
      }, []);
    }

    return [];
  };

  /**
   * Returns all events in date range [fromDate, toDate[ offset'ed with fromDate and toDate
   * Provide your custom events or use events in the calendar (by passing null)
   * @param childId
   * @param fromDate
   * @param toDate
   * @param events
   * @param unitOfTime
   * @return {Array.<*>}
   */
  getEventsInInterval = (childId, fromDate, toDate, events = null) => {
    const mFromDate = moment(fromDate);
    const mToDate = moment(toDate);
    const range = moment.range(mFromDate, mToDate);
    const es = events || this.data.events;
    if (es) {
      return es.reduce((result, event) => {
        const dtStart = event.allDay
          ? moment(event.dtStart)
              .startOf('day')
              .toDate()
          : event.dtStart;
        const dtEnd = event.allDay
          ? moment(event.dtEnd)
              .endOf('day')
              .toDate()
          : event.dtEnd;

        if (!event.rruleset && dtEnd < fromDate) {
          return result;
        }
        if (childId && !event.children.includes(childId)) {
          return result;
        }
        if (event.rruleset) {
          const dates = event.rruleset.between(
            mFromDate.toDate(),
            mToDate.toDate(),
            true,
          );
          dates.forEach((date) => {
            result.push({
              ...event,
              dtStart: new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate(),
                dtStart.getHours(),
                dtStart.getMinutes(),
              ),
              dtEnd: new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate(),
                dtEnd.getHours(),
                dtEnd.getMinutes(),
              ),
            });
          });
          return result;
        }

        const eventRange = moment.range(moment(dtStart), moment(dtEnd));

        const overlap = range.intersect(eventRange);
        if (overlap) {
          const start = overlap.start.toDate();
          const end = overlap.end.toDate();
          const newEvent = {
            ...event,
            origDtStart: dtStart,
            origDtEnd: dtEnd,
            dtStart: start,
            dtEnd: end,
          };
          result.push(newEvent);
        }

        return result;
      }, []);
    }

    return [];
  };

  custodyPlanForChild = childId =>
    this.data.custodyPlans.find(p => p.childId === childId);

  getCustodyDatesForUser = (userId, fromDate, toDate) => {
    if (!('custodyPlans' in this.data)) return [];
    return this.data.custodyPlans.reduce((dates, plan) => {
      const planDates = plan.getCustodyDatesForUser(userId, fromDate, toDate);
      return _.uniq(dates.concat(planDates));
    }, []);
  };

  getWhoHasCustodyForChildren = (children, fromDate, toDate) =>
    children.reduce((users, child) => {
      const plan = this.custodyPlanForChild(child);
      if (plan) {
        const custodyUsers = plan.getWhoHasCustody(fromDate, toDate);
        return _.union(users, custodyUsers);
      }
      return users;
    }, []);

  getWhoHasCustodyForEvent = (event) => {
    const custodyRoles = event.roles.filter(
      role => role.transparentCustody === false && role.custodyRole === 'Pilot',
    );
    if (custodyRoles.length) return custodyRoles.map(role => role.user);
    return event.children.reduce((users, child) => {
      const plan = this.custodyPlanForChild(child);
      if (plan) {
        const custodyUsers = plan.getWhoHasCustody(event.dtStart, event.dtEnd);
        return _.union(users, custodyUsers);
      }
      return users;
    }, []);
  };

  /**
   * Returns the latest active date range for a child (within maxDaysBack)
   * @param userId
   * @param childId
   * @param fromDate
   * @param maxDaysBack
   * @returns {Array}
   */
  getLatestActiveBlock(
    userId,
    childId,
    fromDate = moment().endOf('day'),
    maxDaysBack = 30,
  ) {
    const toDate = moment(fromDate)
      .add(-maxDaysBack, 'day')
      .startOf('day');
    const custodyPlan = this.custodyPlanForChild(childId);
    if (!custodyPlan) return null;

    const dates = custodyPlan.getCustodyDatesForUser(userId, toDate, fromDate);
    if (dates.length === 0) return null;
    // find latest range
    // sort descending
    const sortedDates = dates.sort((a, b) => b - a);
    const range = [];
    sortedDates.every((date) => {
      if (range.length) {
        const latestDate = range[range.length - 1];
        const diff = moment(latestDate).diff(date, 'days');
        if (diff > 1) {
          return false;
        }
      }
      range.push(date);
      return true;
    });

    const from = range.pop();
    const to = range.length ? range.shift() : from;
    return {
      from: moment(from)
        .startOf('day')
        .toDate(),
      to: moment(to)
        .endOf('day')
        .toDate(),
    };
  }

  getWhichChildrenUserHasCustodyFor = (userId, fromDate, toDate) =>
    this.custodyPlans.reduce((result, plan) => {
      const custodyUsers = plan.getWhoHasCustody(fromDate, toDate);
      if (custodyUsers.includes(userId)) {
        result.push(plan.childId);
      }
      return result;
    }, []);

  hasCustodyChange = (events, date = null) =>
    events.some((event) => {
      const start = date ? moment(date).startOf('day') : event.dtStart;
      const end = date ? moment(date).endOf('day') : event.dtEnd;
      const planPilots = this.getWhoHasCustodyForChildren(
        event.children,
        start,
        end,
      );
      const pilots = event.roles
        .filter(role => role.custodyRole === 'Pilot')
        .filter(role => planPilots.find(pp => pp !== role.user))
        .filter(role => !role.transparentCustody);
      return pilots.length !== 0;
    });

  filterEventsWithCustodyChange = (events, date = null) =>
    events.filter((event) => {
      const start = date ? moment(date).startOf('day') : event.dtStart;
      const end = date ? moment(date).endOf('day') : event.dtEnd;
      const planPilots = this.getWhoHasCustodyForChildren(
        event.children,
        start,
        end,
      );
      const pilots = event.roles
        .filter(role => role.custodyRole === 'Pilot')
        .filter(role => planPilots.find(pp => pp !== role.user))
        .filter(role => !role.transparentCustody);
      return pilots.length !== 0;
    });

  constructor(data) {
    if (data) {
      this.data = data;
    }
  }

  toObject = () => {
    const obj = { ...this.data };
    if (!('custodyPlans' in obj)) obj.custodyPlans = [];
    obj.custodyPlans = obj.custodyPlans.map(p => p.toObject());
    obj.events = obj.events.map((event) => {
      let rruleset;
      if (event.rruleset) {
        const rstr = event.rruleset.toString();
        rruleset = rstr.replace('WKST=0', 'WKST=MO');
      }
      return {
        ...event,
        rruleset,
      };
    });
    return obj;
  };

  static fromObject = (obj) => {
    const data = transformProps({ ...obj }, prop => prop && prop.toString(), [
      '_id',
      'childId',
    ]);
    if (data.custodyPlans) {
      data.custodyPlans = data.custodyPlans.map(p => CustodyPlan.fromObject(p));
    }
    if (data.events) {
      data.events = data.events.map(event => ({
        ...event,
        dtStart: new Date(event.dtStart),
        dtEnd: event.dtEnd ? new Date(event.dtEnd) : null,
        rruleset: event.rruleset
          ? rrulestr(event.rruleset, {
            cache: true,
            forceset: true,
            dtstart: new Date(event.dtStart),
          })
          : null,
      }));
    } else {
      data.events = [];
    }
    return new Calendar(data);
  };

  /**
   * Pretty print event timeStr
   * @param event
   * @param currentDate
   * @param translations
   * @param locale
   * @return {string}
   * @constructor
   */
  static PPEventTimeRange = (event, currentDate, translations = {}, locale) => {
    // if (locale) moment.locale(locale); // TODO: https://github.com/moment/moment/issues/3624#issuecomment-335190847
    const trans = {
      today: 'Today',
      'all-day': 'All day',
    };
    Object.assign(trans, translations);
    const mStart = moment(event.dtStart);
    const mEnd = moment(event.dtEnd);
    const mDay = moment(currentDate);
    let startStr = '';
    let endStr = '';

    if (event.allDay) {
      if (mStart.isSame(mEnd, 'day')) {
        return `${mStart.format('DD MMM YYYY')} - ${trans['all-day']}`;
      }
      return `${mStart.format('DD MMM YYYY')}-${mEnd.format('DD MMM')} - ${
        trans['all-day']
      }`;
    }

    startStr = mStart.format('DD MMM YYYY, LT');

    if (mStart.isSame(mEnd, 'day')) {
      endStr = mEnd.format('LT');
    } else {
      endStr = mEnd.format('DD MMM YYYY, LT');
    }

    return `${startStr} - ${endStr}`;
  };
}
