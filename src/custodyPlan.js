import _ from 'lodash';
import {extendMoment} from 'moment-range';

import Moment from 'moment/min/moment-with-locales';
import MultiRange from 'multi-integer-range';

Moment.locale('da');

const moment = extendMoment(Moment);
const assert = require('assert');

/**
 * Custody blocks
 */

class Block {
  data = {};

  get id() {
    return this.data._id;
  }

  get createdAt() {
    return this.data.createdAt;
  }

  get startDate() {
    return this.data.startDate;
  }

  get endDate() {
    return this.data.endDate;
  }

  get minimumDate() {
    return this.data.minimumDate;
  }

  get acceptedBy() {
    return this.data.acceptedBy;
  }

  set id(id) {
    this.data._id = id;
  }

  get title() {
    return this.data.title;
  }

  set title(title) {
    this.data.title = title;
  }

  _setStartDate = startDate => {
    if (!this.data.startDate) {
      this.data.startDate = new Date(8640000000000000);
    }
    this.data.minimumDate = this.data.startDate = moment
      .min(moment(startDate), moment(this.data.startDate))
      .toDate();
  };

  _setEndDate = endDate => {
    if (!this.data.endDate) {
      this.data.endDate = new Date(-8640000000000000);
    }
    this.data.endDate = moment
      .max(moment(endDate), moment(this.data.endDate))
      .toDate();
  };

  /**
   * Returns array of dates where user w. userId has custody in range [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   */
  range(userId, fromDate, toDate) {
    throw new Error('You have to implement the method range');
  }

  /**
   * Returns array of minutes intervals where user w. userId has custody in range [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   */
  intervals(userId, fromDate, toDate) {
    throw new Error('You have to implement the method intervals');
  }

  /**
   * Fill empty dates with userId
   * @param userId
   */
  fillEmptyDates(userId) {
    throw new Error('You have to implement the method fillEmptyDates');
  }

  toObject = () => {
    const obj = {...this.data};
    return obj;
  };
}

class RecurringBlock extends Block {
  static BLOCK_TYPE_RECUR = 2;

  /**
   * Creates an recurring block of {weekCount} weeks, starting a {startDate} and {endDate}
   * includes {startDate} and {endDate} in the block
   * if (endDate) isnt defined the custody plan will recur forever.
   * Minimum/Maximum date is to overcome the issue where you want to start a recurring block
   * *off* the repeating date pattern. If defined the pattern is capped by min and max dates
   * @param userId
   * @param weekCount
   * @param startDate
   * @param endDate
   * @param minimumDate
   * @param maximumDate
   * @param pickUpTime
   * @param dropOffTime
   * @returns recurring block
   */
  constructor(
    userId,
    weekCount,
    startDate,
    endDate = null,
    minimumDate = null,
    maximumDate = null,
    pickUpTime = null,
    dropOffTime = null,
  ) {
    super();
    const weeks = {};
    for (let i = 0; i < weekCount; i += 1) {
      weeks[i] = {};
    }
    if (!minimumDate) {
      minimumDate = moment(startDate).toDate();
    }
    this.data = {
      createdBy: userId,
      acceptedBy: [userId],
      type: RecurringBlock.BLOCK_TYPE_RECUR,
      startDate,
      endDate,
      weekCount,
      weeks,
      minimumDate,
      maximumDate,
      pickUpTime,
      dropOffTime,
      createdAt: new Date(),
    };
  }

  get weekCount() {
    return this.data.weekCount;
  }

  _setUserWeekBitArray = (userId, date, op) => {
    assert(userId !== undefined, 'Please pass userId');
    const mDate = moment(date);
    const mStartDate = moment(this.data.startDate);
    if (!mDate.isSameOrAfter(mStartDate)) {
      return false;
    }
    const {week, dayBit} = RecurringBlock._getWeekAndDayBit(
      mDate,
      mStartDate,
      this.data.weekCount,
    );
    if (this.data.weeks[week] === undefined) {
      this.data.weeks[week] = {};
    }
    if (this.data.weeks[week][userId] === undefined) {
      this.data.weeks[week][userId] = 0;
    }
    switch (op) {
      case 0: // add
        this.data.weeks[week][userId] |= dayBit;
        break;
      case 1: // toggle
        this.data.weeks[week][userId] ^= dayBit;
        break;
      case 2: // clear
        this.data.weeks[week][userId] &= ~dayBit;
        break;
      default:
        break;
    }

    return true;
  };

  static _getWeekAndDayBit(mDate, mStartDate, weekCount) {
    const week = mDate.diff(mStartDate, 'week') % weekCount;
    const day = mDate.isoWeekday();
    const dayBit = 1 << (day - 1);
    return {
      week,
      dayBit,
    };
  }

  incrementWeek = () => {
    this.data.weekCount += 1;
  };
  decrementWeek = () => {
    if (this.data.weekCount > 1) this.data.weekCount -= 1;
  };
  setStartDate = date => {
    this.data.startDate = date;
  };
  setDate = (userId, date, startTime = null, endTime = null) => {
    this._setUserWeekBitArray(userId, date, 0);
  };

  setDates = (userId, dates) => {
    dates.forEach(date => this.setDate(userId, date));
  };

  fillEmptyDates = userId => {
    this.data.weeks = _.map(this.data.weeks, week => {
      let total = 0;
      _.each(week, (v, k) => {
        if (k !== userId) {
          total += v;
        }
      });

      if (total < 127) {
        week[userId] = 127 - total;
      }
      return week;
    });
  };

  toggleDate = (userId, otherId, date) => {
    this._setUserWeekBitArray(userId, date, 1);
    this._setUserWeekBitArray(otherId, date, 1);
  };

  clearDate = (userId, date) => this._setUserWeekBitArray(userId, date, 2);

  isValid = (userId, date) => {
    const mDate = moment(date);
    const mDateBefore = moment(mDate).add(-1, 'day');
    const mDateAfter = moment(mDate).add(1, 'day');
    const mStartDate = moment(this.data.startDate);
    if (!mDate.isSameOrAfter(mStartDate)) {
      return false;
    }
    const {week, dayBit} = RecurringBlock._getWeekAndDayBit(
      mDate,
      mStartDate,
      this.data.weekCount,
    );
    const {
      week: weekBefore,
      dayBit: dayBitBefore,
    } = RecurringBlock._getWeekAndDayBit(
      mDateBefore,
      mStartDate,
      this.data.weekCount,
    );
    const {
      week: weekAfter,
      dayBit: dayBitAfter,
    } = RecurringBlock._getWeekAndDayBit(
      mDateAfter,
      mStartDate,
      this.data.weekCount,
    );

    try {
      const isValidToday = this.data.weeks[week][userId] & dayBit;
      const isValidDayBefore =
        this.data.weeks[weekBefore][userId] & dayBitBefore;
      const isValidDayAfter = this.data.weeks[weekAfter][userId] & dayBitAfter;
      if (!isValidToday) {
        return false;
      }
      if (isValidToday && isValidDayBefore && isValidDayAfter) {
        return true;
      }
      let isValid = true;
      if (this.data.pickUpTime && isValidToday && !isValidDayBefore) {
        isValid =
          mDate.format('HHmm') >= moment(this.data.pickUpTime).format('HHmm');
      }
      if (this.data.dropOffTime && isValidToday && !isValidDayAfter) {
        isValid =
          mDate.format('HHmm') <= moment(this.data.dropOffTime).format('HHmm');
      }
      return isValid;
    } catch (e) {
      return false;
    }
  };

  isDayInWeekSet = (userId, isoDayNo, weekNo) => {
    const dayBit = 1 << (isoDayNo - 1);
    return (
      this.data.weeks[weekNo] &&
      this.data.weeks[weekNo][userId] &&
      this.data.weeks[weekNo][userId] & dayBit
    );
  };

  /**
   * Returns array of minutes intervals where user w. userId has custody in range [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   */
  intervals(userId, fromDate, toDate) {
    const dates = this.range(userId, fromDate, toDate);
    return dates.map(date => [
      moment(date)
        .startOf('day')
        .toDate(),
      moment(date)
        .endOf('day')
        .toDate(),
    ]);
  }

  /**
   * returns dates where userId has custody in interval [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   * @return {Array}
   */
  range = (userId, fromDate, toDate, ignoreMinimumDate = false) => {
    const {startDate, endDate, minimumDate, maximumDate} = this.data;
    if (moment(startDate).isAfter(toDate)) {
      return [];
    }
    if (
      !ignoreMinimumDate &&
      minimumDate &&
      moment(minimumDate).isAfter(toDate)
    ) {
      return [];
    }
    if (endDate && moment(fromDate).isAfter(endDate)) {
      return [];
    }
    let startRange = startDate > fromDate ? startDate : fromDate;
    let endRange = toDate;
    if (endDate && endDate < toDate) {
      endRange = endDate;
    }
    if (!ignoreMinimumDate && minimumDate && startRange < minimumDate) {
      startRange = minimumDate;
    }
    if (maximumDate && endRange > maximumDate) {
      endRange = maximumDate;
    }
    const range = moment.range(moment(startRange), moment(endRange));
    const dates = [];
    for (const date of range.by('day')) {
      if (this.isValid(userId, date)) dates.push(date.toDate());
    }
    return dates;
  };

  /**
   * return array of users who has custody in interval [fromDate, toDate]
   * @param fromDate
   * @param toDate
   * @return {*}
   */
  usersInRange = (fromDate, toDate) => {
    if (moment(this.data.startDate).isAfter(toDate)) {
      return [];
    }
    if (this.data.endDate && moment(fromDate).isAfter(this.data.endDate)) {
      return [];
    }
    const startRange =
      this.data.startDate > fromDate ? this.data.startDate : fromDate;
    let endRange = toDate;
    if (this.data.endDate && this.data.endDate < toDate) {
      endRange = this.data.endDate;
    }
    const range = moment.range(moment(startRange), moment(endRange));
    const weeks = this.data.weeks;
    const mStartDate = moment(this.data.startDate);
    const users = new Set();
    for (const mDate of range.by('day')) {
      const {week, dayBit} = RecurringBlock._getWeekAndDayBit(
        mDate,
        mStartDate,
        this.data.weekCount,
      );
      if (weeks[week]) {
        _.forEach(weeks[week], (v, k) => {
          if (v & dayBit) {
            users.add(k);
          }
        });
      }
    }
    return Array.from(users);
  };

  static fromObject = obj => {
    const rb = new RecurringBlock();
    rb.data = {...obj};
    rb.data.startDate = new Date(rb.data.startDate);
    rb.data.endDate = rb.data.endDate ? new Date(rb.data.endDate) : null;
    rb.data.createdAt = new Date(rb.data.createdAt);
    rb.data.minimumDate = new Date(rb.data.minimumDate);
    rb.data.maximumDate = rb.data.maximumDate
      ? new Date(rb.data.maximumDate)
      : null;
    return rb;
  };
}

class RangeBlock extends Block {
  static BLOCK_TYPE_RANGE = 1;

  constructor(childId, userId) {
    super();
    this.type = RangeBlock.BLOCK_TYPE_RANGE;
    this.data = {
      childId,
      createdBy: userId,
      type: RangeBlock.BLOCK_TYPE_RANGE,
    };

    this.data.ranges = [];
  }

  /**
   * Private
   */
  _isAdjacent = (r1, r2) => {
    const sameStartEnd = Math.abs(r1.start.diff(r2.end)) <= 1;
    const sameEndStart = Math.abs(r1.end.diff(r2.start)) <= 1;
    return (
      (sameStartEnd && r2.start.valueOf() <= r1.start.valueOf()) ||
      (sameEndStart && r2.end.valueOf() >= r1.end.valueOf())
    );
  };
  _mergeAdjacent = (r1, r2) =>
    moment.range(moment.min(r1.start, r2.start), moment.max(r1.end, r2.end));
  _mergeAdjacents = ranges => {
    const self = this;
    return ranges.reduce((result, range) => {
      if (!result) {
        result = range;
      } else {
        result = self._mergeAdjacent(result, range);
      }
      return result;
    });
  };

  _setStartAndEndDate = () => {
    this.data.startDate = null;
    this.data.endDate = null;
    this.data.ranges.forEach(range => {
      if (
        !this.data.startDate ||
        this.data.startDate > range.mrange.start.toDate()
      ) {
        this.data.startDate = range.mrange.start.toDate();
        this.data.minimumDate = this.data.startDate;
      }
      if (!this.data.endDate || this.data.endDate < range.mrange.end.toDate()) {
        this.data.endDate = range.mrange.end.toDate();
      }
    });
  };
  /**
   * unsets custody for date range (date)
   * @return {{this}}
   * @param date
   */
  _clearCustodyForDate = date => {
    const range = this.data.ranges.find(r => r.mrange.contains(date));

    if (range) {
      const start = moment(date)
        .add(-1, 'day')
        .endOf('day')
        .toDate();
      const end = moment(date)
        .add(1, 'day')
        .startOf('day')
        .toDate();
      const r = moment.range(start, end);

      const newRanges = range.mrange.subtract(r);
      // remove old and add new
      this.data.ranges = this.data.ranges.filter(r => r !== range);
      // add new
      newRanges.forEach(r =>
        this.data.ranges.push({
          mrange: r,
          userId: range.userId,
        }),
      );
      this._setStartAndEndDate();
    }
  };

  /**
   * Fill empty dates with userId
   * @param userId
   */
  fillEmptyDates(userId) {
    const emptyDates = [];
    const range = moment.range(this.startDate, this.endDate);
    for (const day of range.by('day')) {
      const empty = !this.data.ranges.some(r => r.mrange.contains(day));
      if (empty) emptyDates.push(day);
    }
    // fill
    emptyDates.forEach(date =>
      this.setCustodyForDate(userId, date.startOf('day'), date.endOf('day')),
    );
  }

  /**
   * Sets custody for date range. If endDate is not given it only sets one day
   * @param userId
   * @param startDate
   * @param endDate
   * @return {{this}}
   */
  setCustodyForDate = (userId, startDate, endDate = null) => {
    this._clearCustodyForDate(startDate); // todo clear entire range
    const e = endDate === null ? startDate : endDate;
    const start = moment(startDate)
      .startOf('day')
      .toDate();
    const end = moment(e)
      .endOf('day')
      .toDate();
    const r = {
      mrange: moment.range(start, end),
      userId,
    };
    // if range overlaps or is adjacent to any other range(s) then merge them

    const adjacents = this.data.ranges.filter(
      o =>
        o.userId === userId &&
        (this._isAdjacent(o.mrange, r.mrange) || o.mrange.intersect(r.mrange)),
    );

    if (adjacents.length) {
      this.data.ranges = this.data.ranges.filter(
        range => adjacents.indexOf(range) === -1,
      );
      r.mrange = this._mergeAdjacents(
        [r.mrange].concat(adjacents.map(range => range.mrange)),
      );
      this.data.ranges.push(r);
    } else {
      this.data.ranges.push(r);
    }
    this._setStartAndEndDate();
    return this;
  };

  /**
   * return array of users who has custody in interval [fromDate, toDate]
   * @param fromDate
   * @param toDate
   * @return {*}
   */
  usersInRange = (fromDate, toDate) => {
    if (moment(this.data.startDate).isAfter(toDate)) {
      return [];
    }
    if (this.data.endDate && moment(fromDate).isAfter(this.data.endDate)) {
      return [];
    }
    const range = moment.range(fromDate, toDate);
    return _(this.data.ranges)
      .filter(r => r.mrange.intersect(range))
      .map(r => r.userId)
      .value();
  };

  /**
   * returns dates where userId has custody in interval [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   * @return {Array}
   */
  range = (userId, fromDate, toDate) => {
    if (moment(this.data.startDate).isAfter(toDate)) {
      return [];
    }
    if (this.data.endDate && moment(fromDate).isAfter(this.data.endDate)) {
      return [];
    }
    const range = moment.range(fromDate, toDate);
    const userRanges = _(this.data.ranges)
      .filter(r => r.userId === userId && r.mrange.intersect(range))
      .value();
    return _(userRanges)
      .map(r => Array.from(r.mrange.by('day')))
      .flatten()
      .map(m => m.toDate())
      .value();
  };

  intervals = (userId, fromDate, toDate) => {
    return []; // todo get rid of RangeBlock
  }

  static fromObject = obj => {
    const rb = new RangeBlock();
    rb.data = {...obj};
    rb.data.startDate = new Date(rb.data.startDate);
    rb.data.endDate = rb.data.endDate ? new Date(rb.data.endDate) : null;
    rb.data.createdAt = new Date(rb.data.createdAt);
    rb.data.ranges = rb.data.ranges.map(range => ({
      ...range,
      mrange: moment.range(
        new Date(range.mrange.start),
        new Date(range.mrange.end),
      ),
    }));
    return rb;
  };
}

class CustodyBlock extends Block {
  static BLOCK_TYPE = 3;

  /**
   *
   */
  constructor({
    userId,
    blocks,
    startDate,
    endDate,
    repeat,
    title,
    sharedId,
    childrenIds,
    createdAt,
  }) {
    super();

    const totalDays = this._totalDaysInBlocks(blocks);
    let calculatedEndDate = endDate;
    if (!calculatedEndDate) {
      if (!repeat) {
        const lastBlock = blocks[blocks.length - 1];
        calculatedEndDate = moment(startDate)
          .add(totalDays, 'days')
          .hours(lastBlock.endTime)
          .minutes(0)
          .toDate();
      }
    }
    const calculatedStartDate = blocks.length
      ? moment(startDate)
          .startOf('day')
          .add(blocks[0].startTime, 'hours')
          .toDate()
      : startDate;
    const minimumDate = moment(calculatedStartDate).toDate();
    this.data = {
      sharedId,
      childrenIds,
      createdBy: userId,
      acceptedBy: [userId],
      type: CustodyBlock.BLOCK_TYPE,
      startDate: calculatedStartDate,
      endDate: calculatedEndDate,
      minimumDate,
      title,
      repeat,
      blocks,
      totalDays,
      createdAt,
    };
  }

  _blockLengthInMinutes = block => {
    const startTime = moment(new Date(0))
      .hour(block.startTime)
      .toDate();
    const endTime = moment(new Date(0))
      .add(block.days, 'days')
      .hour(block.endTime)
      .toDate();
    return (endTime - startTime) / (1000 * 60);
  };
  _lengthInMinutes = () => {
    const totalDays = this.data.blocks.reduce(
      (count, block) => count + block.days,
      0,
    );
    const bStartTime = this.data.blocks[0].startTime;
    const bEndTime = this.data.blocks[this.data.blocks.length - 1].endTime;
    const startTime = moment(new Date(0))
      .hour(bStartTime)
      .toDate();
    const endTime = moment(new Date(0))
      .add(totalDays, 'days')
      .hour(bEndTime)
      .toDate();
    return (endTime - startTime) / (1000 * 60);
  };

  _blockStartInMinutes = block => {
    let minutes = 0;
    for (const b of this.data.blocks) {
      if (b.id === block.id) {
        minutes += block.startTime * 60;
        break;
      }
      minutes += b.days * 24 * 60;
    }
    return minutes;
  };

  /*  if (!block.prevBlock) return block.startTime * 60;
    return (
      this._blockStartInMinutes(block.prevBlock) +
      this._blockLengthInMinutes(block.prevBlock)
    );
  };*/
  _totalDaysInBlocks = blocks =>
    blocks.reduce((count, block) => count + block.days, 0);

  _getIntersections = (fromDate, toDate) => {
    const mStartDate = moment(this.data.startDate);
    const mToDate = moment(toDate);
    const mFromDate = moment(fromDate);

    if (mToDate.isBefore(mStartDate)) {
      return [];
    }
    if (this.data.endDate && mFromDate.isAfter(moment(this.data.endDate))) {
      return [];
    }

    // normalize relatively to start of the block day
    const mStartOfDay = moment(mStartDate).startOf('day'); // 0
    const diffInDays = mFromDate.diff(mStartOfDay, 'days');
    let diffStart = mFromDate.diff(mStartOfDay, 'minutes'); // 6
    let diffEnd = mToDate.diff(mStartOfDay, 'minutes'); // 8

    const lengthInMinutesSinceStart = this.data.totalDays * 60 * 24;
    if (this.data.repeat) {
      if (lengthInMinutesSinceStart > 0) {
        diffStart %= lengthInMinutesSinceStart;
        diffEnd %= lengthInMinutesSinceStart;
      }
    }

    const ranges = new MultiRange();
    if (diffEnd < diffStart) {
      const start = this._blockStartInMinutes(this.data.blocks[0]);
      const end = lengthInMinutesSinceStart;
      ranges.append([[start, start + diffEnd]]);
      ranges.append([[diffStart, end]]);
    } else {
      const range = [[diffStart, diffEnd]];
      ranges.append(range);
    }

    let intersections = [];
    this.data.blocks.forEach(block => {
      const blockStart = this._blockStartInMinutes(block);
      const blockEnd = blockStart + this._blockLengthInMinutes(block);
      const blockRange = new MultiRange([[blockStart, blockEnd]]);
      const intersection = blockRange.intersect(ranges);
      if (intersection.length()) {
        const ranges = [];
        intersection.ranges.forEach(range => {
          const blockNo = Math.floor(diffInDays / this.data.totalDays);
          const blockDays = (blockNo < 0 ? 0 : blockNo) * this.data.totalDays;
          // correct date time
          const correctRange = [
            moment(mStartOfDay)
              .add(blockDays, 'days')
              .add(range[0], 'minutes')
              .toDate(),
            moment(mStartOfDay)
              .add(blockDays, 'days')
              .add(range[1], 'minutes')
              .endOf('minute')
              .toDate(),
          ];
          ranges.push(correctRange);
        });
        intersections = [
          ...intersections,
          {
            block,
            ranges,
          },
        ];
      }
    });
    return intersections;
  };

  /**
   * return array of users who has custody in interval [fromDate, toDate]
   * @param fromDate
   * @param toDate
   * @return {*}
   */
  usersInRange = (fromDate, toDate) => {
    const intersections = this._getIntersections(fromDate, toDate);
    let users = [];
    intersections.forEach(intersection => {
      users = [...users, ...intersection.block.users];
    });
    return users;
  };

  /**
   * Returns array of dates where user w. userId has custody in range [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   */
  range(userId, fromDate, toDate) {
    if (moment(this.data.startDate).isAfter(toDate)) {
      return [];
    }
    if (this.data.endDate && moment(fromDate).isAfter(this.data.endDate)) {
      return [];
    }
    const startRange =
      this.data.startDate > fromDate ? this.data.startDate : fromDate;
    let endRange = toDate;
    if (this.data.endDate && this.data.endDate < toDate) {
      endRange = this.data.endDate;
    }
    const range = moment.range(moment(startRange), moment(endRange));

    const dates = [];
    for (const mDate of range.by('day')) {
      const users = this.usersInRange(
        moment(mDate).startOf('day'),
        moment(mDate).endOf('day'),
      );
      if (users.includes(userId)) dates.push(mDate.toDate());
    }
    return dates;
  }

  /**
   * Returns array of date intervals where user w. userId has custody in range [fromDate, toDate]
   * @param userId
   * @param fromDate
   * @param toDate
   */
  intervals(userId, fromDate, toDate) {
    if (moment(this.data.startDate).isAfter(toDate)) {
      return [];
    }
    if (this.data.endDate && moment(fromDate).isAfter(this.data.endDate)) {
      return [];
    }
    const startRange =
      this.data.startDate > fromDate ? this.data.startDate : fromDate;
    let endRange = moment(toDate).toDate();
    if (this.data.endDate && this.data.endDate < toDate) {
      endRange = this.data.endDate;
    }

    const range = moment.range(startRange, endRange);

    let intervals = [];
    for (const date of range.by('day')) {
      const intersections = this._getIntersections(
        moment(date)
          .startOf('day')
          .toDate(),
        moment(date)
          .endOf('day')
          .toDate(),
      );

      const ranges = intersections.reduce((ranges, intersection) => {
        if (intersection.block.users.includes(userId)) {
          for (const range of intersection.ranges) {
            ranges.push(range);
          }
        }
        return ranges;
      }, []);
      intervals = [...intervals, ...ranges];
    }

    return intervals;
  }

  /**
   * Fill empty dates with userId
   * @param userId
   */
  fillEmptyDates(userId) {
    throw new Error('You have to implement the method fillEmptyDates');
  }

  toObject = () => {
    const obj = {
      ...this.data,
      topic: {
        title: this.data.title,
      },
      blocks: this.data.blocks.map(b => ({
        ...b,
        prevBlock: b.prevBlock ? b.prevBlock.id : undefined,
      })),
    };
    return obj;
  };

  static fromObject = obj => {
    const rb = new CustodyBlock({
      ...obj,
      startDate: new Date(obj.startDate),
      endDate: obj.endDate ? new Date(obj.endDate) : null,
      createdAt: new Date(obj.createdAt),
      blocks: obj.blocks.map(b => {
        const newBlock = {...b};
        if (b.prevBlock) {
          const refBlock = obj.blocks.find(rb => rb.id === b.prevBlock);
          newBlock.prevBlock = {
            ...refBlock,
          };
        }
        return newBlock;
      }),
    });

    return rb;
  };
}

/**
 * CustodyPlan -
 */
export default class CustodyPlan {
  data = {
    blocks: [],
  };

  /**
   * constructor
   */
  constructor(childId) {
    this.data.childId = childId;
  }

  get childId() {
    return this.data.childId;
  }

  get id() {
    return this.data._id;
  }

  get updatedAt() {
    return this.data.updatedAt;
  }

  get blocks() {
    return this.data.blocks;
  }

  _getBlockSweep(fromDate, toDate) {
    const self = this;
    if (!fromDate) fromDate = moment();
    if (!toDate) {
      toDate = moment(fromDate).endOf('day');
    }
    const range = moment.range(fromDate, toDate);
    let currentBlock = null;
    const sweep = [];
    for (const day of range.by('day')) {
      const block = self.getActiveBlock(day);
      if (block && currentBlock !== block) {
        currentBlock = block;
        sweep.push({
          block: currentBlock,
          day,
        });
      }
    }
    return sweep;
  }

  /**
   * Factory
   */
  createRecurringBlock = (...args) => new RecurringBlock(...args);
  createRangeBlock = (...args) => new RangeBlock(this.data.childId, ...args);
  createCustodyBlock = (...args) => new CustodyBlock(...args);

  /**
   * Adds block to the custody plan
   * @param block
   */
  addBlock = block => {
    block.sortId = this.data.blocks.length;
    this.data.blocks.push(block);
    return this;
  };

  /**
   * Returns caretaker who has custody in [fromDate, toDate]
   * @param fromDate
   * @param toDate
   */
  getWhoHasCustody = (fromDate, toDate) => {
    const sweep = this._getBlockSweep(fromDate, toDate);

    let users = [];
    for (let i = 0; i < sweep.length; i += 1) {
      const to =
        i === sweep.length - 1
          ? toDate
          : moment(sweep[i + 1].day)
              .add(-1, 'day')
              .endOf('day');
      const from = sweep[i].day;
      const block = sweep[i].block;
      const usersInRange = block.usersInRange(from, to);
      users = users.concat(usersInRange);
    }
    return users;
  };

  /**
   * Returns the dates a caretaker with userId has custody after from date and before toDate
   * @param userId
   * @param fromDate
   * @param toDate
   * @return [dates]
   */
  getCustodyDatesForUser = (userId, fromDate, toDate) => {
    const sweep = this._getBlockSweep(fromDate, toDate);

    let dates = [];
    for (let i = 0; i < sweep.length; i += 1) {
      const to =
        i === sweep.length - 1
          ? toDate
          : moment(sweep[i + 1].day)
              .add(-1, 'day')
              .endOf('day');
      const from = sweep[i].day;
      const block = sweep[i].block;
      const custodyDates = block.range(userId, from, to);
      dates = dates.concat(custodyDates);
    }
    return dates;
  };

  /**
   * Returns a range of time intervals (minutes from fromDat) a caretaker with userId has custody after from date and before toDate
   * @param userId
   * @param fromDate
   * @param toDate
   * @return [[fromMin, toMin], [fromMinut, ...]]
   */
  getCustodyDateIntervalsPerDayForUser = (userId, fromDate, toDate) => {
    const sweep = this._getBlockSweep(fromDate, toDate);

    let dates = [];
    for (let i = 0; i < sweep.length; i += 1) {
      const to =
        i === sweep.length - 1
          ? toDate
          : moment(sweep[i + 1].day)
              .add(-1, 'day')
              .endOf('day');
      const from = sweep[i].day;
      const block = sweep[i].block;
      const custodyDates = block.intervals(userId, from, to);
      dates = dates.concat(custodyDates);
    }
    return dates;
  };

  /**
   * Get the active for a date and a child
   * If childId is not given return for all children.
   * Use blockType to filter all blocks away that are not of type <blockType>
   * @param date
   * @param blockType
   */
  getActiveBlock = (date, blockType = null) => {
    const blocks = this.getActiveBlocks(date, blockType);
    if (blocks.length) {
      return blocks.sort(
        (a, b) =>
          a.data.repeat - b.data.repeat ||
          new Date(b.data.createdAt) - new Date(a.data.createdAt),
      )[0];
    }
    return null;
  };

  /**
   * return list of active custody blocks for a certain date.
   * Use blockType to filter all blocks away that are not of type <blockType>
   * @param date
   * @param blockType
   * @returns {*[]}
   */
  getActiveBlocks = (date, blockType = null, userId = null) =>
    this.getActiveBlocksInRange(
      moment(date)
        .startOf('day')
        .toDate(),
      moment(date)
        .endOf('day')
        .toDate(),
      blockType,
    );

  /**
   * return list of active custody blocks for a certain interval.
   * Use blockType to filter all blocks away that are not of type <blockType>
   * @param fromDate
   * @param toDate
   * @param blockType
   * @returns {*[]}
   */
  getActiveBlocksInRange = (
    fromDate,
    toDate,
    blockType = null,
    userId = null,
  ) =>
    this.data.blocks.filter(block => {
      // discard wrong type
      if (blockType && blockType !== block.data.type) {
        return false;
      }

      if (
        !block.minimumDate ||
        moment(block.minimumDate).isSameOrBefore(toDate)
      ) {
        if (!block.endDate || moment(block.endDate).isSameOrAfter(fromDate)) {
          // discard range block "holes"
          if (block.data.type === RangeBlock.BLOCK_TYPE_RANGE) {
            const startDate = moment(fromDate)
              .startOf('day')
              .toDate();
            const endDate = moment(toDate)
              .endOf('day')
              .toDate();
            const users = block.usersInRange(startDate, endDate);
            if (!users.length) return false;
            if (userId) {
              if (!users.includes(userId)) return false;
            }
          }
          return true;
        }
      }
      return false;
    });
  /**
   * Returns/gets JSON representation of the custody plan
   * @return {JSON}
   */
  toObject = () => {
    const obj = {...this.data};
    obj.blocks = obj.blocks.map(b => b.toObject());
    return obj;
  };

  static fromObject = obj => {
    const data = {...obj};
    const cp = new CustodyPlan(data.childId);
    cp.data = data;

    cp.data.createdAt = new Date(cp.data.createdAt);
    cp.data.updatedAt = new Date(cp.data.updatedAt);
    cp.data.blocks = cp.data.blocks.map(b => {
      switch (b.type) {
        case RangeBlock.BLOCK_TYPE_RANGE:
          return RangeBlock.fromObject(b);
        case RecurringBlock.BLOCK_TYPE_RECUR:
          return RecurringBlock.fromObject(b);
        case CustodyBlock.BLOCK_TYPE:
          return CustodyBlock.fromObject(b);
        default:
          break;
      }
    });
    return cp;
  };
}

export {RecurringBlock, RangeBlock, CustodyBlock};
