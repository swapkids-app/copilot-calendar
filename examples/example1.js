import CustodyPlan from '../src/custodyPlan';
import moment from 'moment'

// Create custody empty plan
let cp = new CustodyPlan();

// Set custody
const now = new Date();
cp.setCustodyForDate(now);

// Unset custody
cp.unsetCustodyForDate(now);

// Create recurring weeks and add to the plan
let recur = cp.createRecurringCustody(2, start, moment("20170501"));
recur.setDate(moment("20170320"));
recur.setDate(moment("20170324"));
recur.setDate(moment("20170329"));
recur.setDate(moment("20170402"));

cp.addReccuringBlockToPlan(recur);

// CONVERT Custody Plan to serializable JSON
console.log(cp.JSON);
