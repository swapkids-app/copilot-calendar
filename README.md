# Why? 
Because Calendar and custody functions are the most _important_ features 
of CoPilots. We use this across a lot of services. It should always be working, tested and easy to share.

Go Crazy


# Examples

### Init
```javascript
import CustodyPlan from 'custodyPlan';

// create custody plan 
let cp = new CustodyPlan();
```

### Set custody for one day

```javascript
const now = new Date();
cp.setCustodyForDay(now);
```

### Unset custody
```javascript
const now = new Date();
cp.unsetCustodyForDay(now);
```
 
 
### Set recurring custody
```javascript
// Create recurring weeks and add to the plan
let recur = cp.createRecurringCustody(2, start, moment("20170501"));
recur.addDate(moment("20170320"));
recur.addDate(moment("20170324"));
recur.addDate(moment("20170329"));
recur.addDate(moment("20170402"));

cp.addReccuringBlockToPlan(recur);
```
 

### Get the custody dates in a specific date interval
```javascript
cp.getCustodyForDates(new Date("1970-01-01"), new Date("2018-01-01"))

```
 
 
### Convert Custody Plan to serializable JSON
```javascript
const JSON = cp.JSON;
const newCustodyPlan = new CustodyPlan(JSON);
assert(cp === newCustodyPlan);
```
 


# Build
* Build with [Babel](https://babeljs.io). (ES6 -> ES5)
* Test with [mocha](https://mochajs.org).
* Cover with [istanbul](https://github.com/gotwarlost/istanbul).
* Check with [eslint](eslint.org).
* Deploy with [Travis](travis-ci.org).

# Commands
- `npm run clean` - Remove `lib/` directory
- `npm test` - Run tests. Tests can be written with ES6 (WOW!)
- `npm test:watch` - You can even re-run tests on file changes!
- `npm run cover` - Yes. You can even cover ES6 code.
- `npm run lint` - We recommend using [airbnb-config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb). It's fantastic.
- `npm run test:examples` - We recommend writing examples on pure JS for better understanding module usage.
- `npm run build` - Do some magic with ES6 to create ES5 code.
- `npm run prepublish` - Hook for npm. Do all the checks before publishing you module.


